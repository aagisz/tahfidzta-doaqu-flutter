import 'dart:async';

import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:tahfidzta_doaqu/base/apps.dart';
import 'package:tahfidzta_doaqu/base/style.dart';
import 'package:tahfidzta_doaqu/page/home/home_view.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  var _imgLogo = 'assets/images/logo.png';

  @override
  void initState() {
    super.initState();
    debug();
    splashTimer();
  }

  splashTimer() async {
    var duration = Duration(seconds: 2);
    return Timer(duration, () {
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
       return HomePage();
      }));
    });
  }

  void debug() {
    print('Launcher was opened!');
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [TahDoPallete.green1, TahDoPallete.green2]),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Image.asset(
              _imgLogo,
              height: 250,
              width: 250,
            ),
            Padding(padding: EdgeInsets.only(top: 16)),
            JumpingDotsProgressIndicator(
              fontSize: FontSize.h2,
              color: Colors.white,
            ),
            Container(
              child: Column(
                children: <Widget>[
                  Text(
                    Base.number,
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w300,
                        color: Colors.white),
                  ),
                  Text(
                    Base.copyright,
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Colors.white),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
