import 'package:flutter/material.dart';
import 'package:tahfidzta_doaqu/base/apps.dart';
import 'package:tahfidzta_doaqu/base/style.dart';

class KalenderIslam extends StatefulWidget {
  @override
  _KalenderIslamState createState() => _KalenderIslamState();
}

class _KalenderIslamState extends State<KalenderIslam> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          Base.appName,
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: TahDoPallete.green1,
        elevation: 4,
      ),
      body: Container(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: Center(child: Text('Under Construction')),
      ),
    );
  }
}
