class ActivityModel {
  String kegiatanImgPath;
  String kegiatanTitle;
  String kegiatanDate;
  String kegiatanContent;

  ActivityModel({this.kegiatanImgPath, this.kegiatanTitle, this.kegiatanDate, this.kegiatanContent});
}