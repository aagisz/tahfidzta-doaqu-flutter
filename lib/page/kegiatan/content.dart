class ActivityContent {
  String imgKegiatan1 = 'assets/images/1.jpg';
  String titleKegiatan1 = 'Ngaji Sambil Bermain';
  String dateKegiatan1 = '12 Febuari 2020';
  String contentKegiatan1 = 'Assalamu\'alaikum teman-teman  Nah kalo ini teman-teman yang ngaji sore lagi bermain bersama di taman pasir!! Seneng banget ya bisa ngeliat teman-teman ceria main diluar!!';

  String imgKegiatan2 = 'assets/images/2.jpg';
  String titleKegiatan2 = 'Hasil Karya Kelas Pagi';
  String dateKegiatan2 = '12 Febuari 2020';
  String contentKegiatan2 = 'Assalamu\'alaikum teman-teman tahfizta Inilah hasil karya dari salah satu teman kita yang dari kelas pagi~ Kreatif ya!!! Eits, tapi jangan khawatir, teman-teman masih tetap murojaah kok';

  String imgKegiatan3 = 'assets/images/3.jpg';
  String titleKegiatan3 = 'Olahraga dan bermain bersama';
  String dateKegiatan3 = '12 Febuari 2020';
  String contentKegiatan3 = 'Assalamu\'alaikum teman-teman Hari ini teman-teman tahfizta main ke taman dong~ Kita olahraga dan bermain bersama!! Tapi ngajinya juga gak lupa kok! Pokoknya hari ini seru banget!!';

  String imgKegiatan4 = 'assets/images/4.jpg';
  String titleKegiatan4 = 'Kelas Mewarnai Agar tidak Jenuh';
  String dateKegiatan4 = '12 Febuari 2020';
  String contentKegiatan4 = 'Assalamu\'alaikum, jum\'at yang mubarok ini anak-anak tahfidzta Doaqu ada kegiatan mewarnai untuk kelas sore supaya tidak jenuh.. Hehe.';

  String imgKegiatan5 = 'assets/images/5.jpg';
  String titleKegiatan5 = 'Hadiah dalam Menghafal';
  String dateKegiatan5 = '12 Febuari 2020';
  String contentKegiatan5 = 'Alhamdulillah Barakallah *Santri Of The Week* yang selalu bunyi disaat murojaah . Semoga hadiahnya bermanfaat dan tambah semangat dalam menghafal Untuk santri yang lain juga tetap semangat yaa dan tunggu hadiah dari bu guru selanjutnya';

  String imgKegiatan6 = 'assets/images/6.jpg';
  String titleKegiatan6 = 'Dokumentasi Rihlah Pertama';
  String dateKegiatan6 = '12 Febuari 2020';
  String contentKegiatan6 = 'Ini dia salah satu dokumentasi rihlah pertama Tahfizta kemarin. Banyak pengalaman baru bersama teman-teman yang membuat kita semua ga bisa berhenti senyum dan tertawa. Semoga setelah rihlah dan liburan teman-teman semakin semangat hafalannya.';

}