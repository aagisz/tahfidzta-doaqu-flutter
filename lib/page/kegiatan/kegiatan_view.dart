import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:tahfidzta_doaqu/base/apps.dart';
import 'package:tahfidzta_doaqu/base/style.dart';
import 'package:tahfidzta_doaqu/page/kegiatan/content.dart';
import 'package:tahfidzta_doaqu/page/kegiatan/detailkegiatan_view.dart';
import 'package:tahfidzta_doaqu/page/kegiatan/kegiatan_model.dart';

class KegiatanPage extends StatefulWidget {
  @override
  _KegiatanPageState createState() => _KegiatanPageState();
}

class _KegiatanPageState extends State<KegiatanPage> {
  void initState() {
    super.initState();
    print('[Page] Kegiatan');
  }

  Future<List<ActivityModel>> _fetchActivityList() async {
    List<ActivityModel> _kegiatanList = [];

    _kegiatanList.add(ActivityModel(
        kegiatanImgPath: ActivityContent().imgKegiatan1,
        kegiatanTitle: ActivityContent().titleKegiatan1,
        kegiatanDate: ActivityContent().dateKegiatan1,
        kegiatanContent: ActivityContent().contentKegiatan1));
    _kegiatanList.add(ActivityModel(
        kegiatanImgPath: ActivityContent().imgKegiatan2,
        kegiatanTitle: ActivityContent().titleKegiatan2,
        kegiatanDate: ActivityContent().dateKegiatan2,
        kegiatanContent: ActivityContent().contentKegiatan2));
    _kegiatanList.add(ActivityModel(
        kegiatanImgPath: ActivityContent().imgKegiatan3,
        kegiatanTitle: ActivityContent().titleKegiatan3,
        kegiatanDate: ActivityContent().dateKegiatan3,
        kegiatanContent: ActivityContent().contentKegiatan3));
    _kegiatanList.add(ActivityModel(
        kegiatanImgPath: ActivityContent().imgKegiatan4,
        kegiatanTitle: ActivityContent().titleKegiatan4,
        kegiatanDate: ActivityContent().dateKegiatan4,
        kegiatanContent: ActivityContent().contentKegiatan4));
    _kegiatanList.add(ActivityModel(
        kegiatanImgPath: ActivityContent().imgKegiatan5,
        kegiatanTitle: ActivityContent().titleKegiatan5,
        kegiatanDate: ActivityContent().dateKegiatan5,
        kegiatanContent: ActivityContent().contentKegiatan5));
    _kegiatanList.add(ActivityModel(
        kegiatanImgPath: ActivityContent().imgKegiatan6,
        kegiatanTitle: ActivityContent().titleKegiatan6,
        kegiatanDate: ActivityContent().dateKegiatan6,
        kegiatanContent: ActivityContent().contentKegiatan6));

    return Future.delayed(Duration(seconds: 5), () {
      return _kegiatanList;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          Base.appName,
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: TahDoPallete.green1,
        elevation: 0,
      ),
      body: ListView(
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
            child: Text('Kegiatan terbaru TAHFIDZTA DOAQU', style: TextStyle(fontSize: FontSize.h6, fontWeight: FontWeight.bold),),
          ),
          FutureBuilder<List>(
            future: _fetchActivityList(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                  shrinkWrap: true,
                  physics: BouncingScrollPhysics(),
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, position) {
                    return bigCard(snapshot.data[position]);
                  },
                );
              }
              return Center(
                child: JumpingDotsProgressIndicator(
                  fontSize: FontSize.h3,
                  color: TahDoPallete.green1,
                ),
              );
            },
          )
        ],
      ),
    ));
  }

  Widget bigCard(ActivityModel kegiatanModel) {
    String title = kegiatanModel.kegiatanTitle;
    String date = kegiatanModel.kegiatanDate;
    String content = kegiatanModel.kegiatanContent;
    String img = kegiatanModel.kegiatanImgPath;
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16, top: 16),
      child: Column(
        children: <Widget>[
          InkWell(
            onTap: () {
              print('Kegiatan $title was pressed');
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          DetailKegiatan(img, title, date, content)));
            },
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  border: Border.all(width: 1, color: Colors.black12),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black12,
                        blurRadius: 4,
                        offset: Offset(0, 4))
                  ]),
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(img), fit: BoxFit.cover),
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(5),
                            topLeft: Radius.circular(5)),
                        color: Colors.black12),
                    height: 100,
                    width: double.infinity,
                  ),
                  Container(
                    padding: EdgeInsets.all(16),
                    child: Column(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            title,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: TextStyle(
                                color: Colors.black87,
                                fontSize: FontSize.body1,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 4),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              date,
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: FontSize.caption),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 4),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              content,
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: Colors.black87,
                                fontSize: FontSize.body2,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 8),
                          child: Align(
                            alignment: Alignment.topRight,
                            child: Text(
                              'Baca Lebih Lanjut..',
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: FontSize.caption,
                                  decoration: TextDecoration.underline),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
