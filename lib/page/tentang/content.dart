class AboutDescription {
  String heading = 'Apa itu Pondok TAHFIDZTA DOAQU?';
  String content = 'Pondok Tahfidzta DOAQU (Tahfidz Balita Doa Ahlul Quran) merupakan program sekolah tahfidz di bawah yayasan "Pondok DOAQU" yang diperuntukkan untuk balita dengan rentan usia 2 sampai 6 tahun. Tahfidzta DOAQU didirikan pada tanggal 18 Februari 2019 dengan tujuan menciptakan santri - santri yang berprestasi berdasarkan iman dan takwa serta berakhlakul karimah\nMetode yang digunakan oleh Tahfidzta DOAQU merupakan metode hafalan Al-Quran yang diadaptasi dari Cairo, Mesir dengan memaksimalkan pendengaran dan talqin. Sebab semua berawal dari mendengar, kita belajar bahasa pun dari mendengarkan. Untuk itu Tahfidzta DOAQU akan membiasakan balita untuk mendengarkan Bahasa Al-Quran sejak dini. Tentunya dengan takaran yang telah disesuaikan dengan masing-masing balita.';
}

class VisionAndMission {
  String vision = 'Menjadi Madrasah yang Qur’ani, unggul dalam ilmu pengetahuan, teguh dalam iman dan taqwa.';
  String mission = 'Mewujudkan pembelajaran dan pembiasaan dalam mempelajari Al-Qur’an dan mengamalkan isi kandungannya.\nMewujudkan pembentukan karakter Qur’ani yang mampu mengaktualisasikan diri dalam masyarakat';
}