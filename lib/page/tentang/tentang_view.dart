import 'package:flutter/material.dart';
import 'package:tahfidzta_doaqu/base/apps.dart';
import 'package:tahfidzta_doaqu/base/style.dart';
import 'content.dart';

class TentangPage extends StatefulWidget {
  @override
  _TentangPageState createState() => _TentangPageState();
}

class _TentangPageState extends State<TentangPage> {
  var _imgLogo = 'assets/images/logo.png';

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          Base.appName,
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: TahDoPallete.green1,
        elevation: 0,
      ),
      body: Container(
          margin: EdgeInsets.only(left: 16, right: 16),
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: <Widget>[
                Image(
                  image: AssetImage(_imgLogo),
                  height: 250,
                  width: 250,
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Apa itu Pondok TAHFIDZTA DOAQU?',
                    style: TextStyle(
                        fontSize: FontSize.h6,
                        fontWeight: FontWeight.bold,
                        color: TahDoPallete.green1),
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 8)),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    AboutDescription().content,
                    style: TextStyle(fontSize: FontSize.body2),
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 16)),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Visi dan Misi TAHFIDZTA DOAQU',
                    style: TextStyle(
                        fontSize: FontSize.h6,
                        fontWeight: FontWeight.bold,
                        color: TahDoPallete.green1),
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 4)),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Visi',
                    style: TextStyle(
                        fontSize: FontSize.body1, fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 4)),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    VisionAndMission().vision,
                    style: TextStyle(
                      fontSize: FontSize.body2,
                    ),
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 8)),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Misi',
                    style: TextStyle(
                        fontSize: FontSize.body1, fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 4)),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    VisionAndMission().mission,
                    style: TextStyle(
                      fontSize: FontSize.body2,
                    ),
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 32))
              ],
            ),
          )),
    ));
  }
}
