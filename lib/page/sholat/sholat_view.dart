import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:tahfidzta_doaqu/api/api_base.dart';
import 'package:tahfidzta_doaqu/api/api_service.dart';
import 'package:tahfidzta_doaqu/base/apps.dart';
import 'package:tahfidzta_doaqu/base/debug.dart';
import 'package:tahfidzta_doaqu/base/style.dart';
import 'package:geolocator/geolocator.dart';
import 'package:tahfidzta_doaqu/page/sholat/sholat_respone.dart' as sholat;

class WaktuSholatPage extends StatefulWidget {
  const WaktuSholatPage(
      {Key key,
      @required this.latitude,
      @required this.longitude,
      @required this.location,
      @required this.fajr,
      @required this.dhuhr,
      @required this.asr,
      @required this.maghrib,
      @required this.isha})
      : super(key: key);
  final String latitude, longitude, location, fajr, dhuhr, asr, maghrib, isha;

  @override
  _WaktuSholatPageState createState() => _WaktuSholatPageState();
}

class _WaktuSholatPageState extends State<WaktuSholatPage> {
  var _currentLocation = '',
      _currentCity = '',
      _latitudeData = '',
      _longitudeData = '';

  var _fajr = '', _dhuhr = '', _asr = '', _maghrib = '', _isha = '--';
  var _currentTime = '';

  var _currentSholat = '', _currentTimeSholat = '';

  var _fajrStatus = false,
      _dhuhrStatus = false,
      _asrStatus = false,
      _maghribStatus = false,
      _ishaStatus = false;

  bool _progressIndicator = true, _showData = false;

  List<sholat.Datetime> _listDateTime = List();

  @override
  void initState() {
    _currentLocation = widget.location;
    _latitudeData = widget.latitude;
    _longitudeData = widget.longitude;

    _fajr = widget.fajr;
    _dhuhr = widget.dhuhr;
    _asr = widget.asr;
    _maghrib = widget.maghrib;
    _isha = widget.isha;

    if (_latitudeData.isEmpty || _longitudeData.isEmpty) {
      _getCurrentLocation();
    } else {
      _getSholatData();
    }
    super.initState();
    print('[Page] Jadwal Sholat');
  }

  Future _getCurrentLocation() async {
    try {
      Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
      geolocator
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
          .then((locationData) async {
        List<Placemark> listPlace = await geolocator.placemarkFromCoordinates(
            locationData.latitude, locationData.longitude);
        Placemark place = listPlace[0];
        setState(() {
          _currentLocation = place.locality;
          _currentCity = place.subAdministrativeArea;
          _latitudeData = locationData.latitude.toString();
          _longitudeData = locationData.longitude.toString();
          _getSholatData();
        });
        Debug().locationData(
            _latitudeData, _longitudeData, _currentLocation, _currentCity);
      }).catchError((error) {
        debugPrint('Geolocator Error: $error');
      });
    } catch (error) {
      print('Get current location error: $error');
    }
  }

  Future _getSholatData() async {
    var urlApi;
    await ApiUrl.sholatSchedule(
            ApiUrl.methodWeek, _latitudeData, _longitudeData)
        .then((value) => urlApi = value);
    ApiService().get(
        url: urlApi,
        headers: {},
        callback: (status, message, respone) {
          setState(() {
            if (_listDateTime.isNotEmpty) {
              _listDateTime.clear();
            }
            if (status) {
              sholat.ResponseSholat responseSholat =
                  sholat.ResponseSholat.fromJson(respone);
              _listDateTime = responseSholat.results.datetime;

              for (var position = 0;
                  position < _listDateTime.length;
                  position++) {
                if (_listDateTime[position].date.gregorian == _currentTime) {
                  setState(() {
                    _fajr = _listDateTime[position].times.fajr;
                    _dhuhr = _listDateTime[position].times.dhuhr;
                    _asr = _listDateTime[position].times.asr;
                    _maghrib = _listDateTime[position].times.maghrib;
                    _isha = _listDateTime[position].times.isha;

                    var _curTime = Func.timeToInt(Func.getTime(Format.time_3));
                    var _iShubuh = Func.timeToInt(_fajr);
                    var _iDhuhur = Func.timeToInt(_dhuhr);
                    var _iAshar = Func.timeToInt(_asr);
                    var _iMaghrib = Func.timeToInt(_maghrib);
                    var _iIsya = Func.timeToInt(_isha);

                    if (_curTime >= _iShubuh && _curTime < _iDhuhur) {
                      _dhuhrStatus = true;
                      _currentTimeSholat = _dhuhr;
                      _currentSholat = Time.dhuhur;
                    } else if (_curTime >= _iDhuhur && _curTime < _iAshar) {
                      _asrStatus = true;
                      _currentTimeSholat = _asr;
                      _currentSholat = Time.ashar;
                    } else if (_curTime >= _iAshar && _curTime < _iMaghrib) {
                      _maghribStatus = true;
                      _currentTimeSholat = _maghrib;
                      _currentSholat = Time.maghrib;
                    } else if (_curTime >= _iMaghrib && _curTime < _iIsya) {
                      _ishaStatus = true;
                      _currentTimeSholat = _isha;
                      _currentSholat = Time.isya;
                    } else {
                      _fajrStatus = true;
                      _currentTimeSholat = _fajr;
                      _currentSholat = Time.shubuh;
                    }
                  });
                }
              }
              Debug().timeSholatData(
                  _currentTime, _fajr, _dhuhr, _asr, _maghrib, _isha);
              _progressIndicator = false;
              _showData = true;
            }
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    _currentTime = Func.getTime(Format.time_4);
    return SafeArea(
        child: Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              title: Text(
                Base.appName,
                style: TextStyle(color: Colors.white),
              ),
              centerTitle: true,
              backgroundColor: TahDoPallete.green1,
              elevation: 0,
              actions: <Widget>[
                IconButton(
                  onPressed: () {
                    _getCurrentLocation();
                  },
                  hoverColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  icon: Icon(
                    Icons.refresh_rounded,
                    color: Colors.white,
                    size: FontSize.h5,
                  ),
                ),
              ],
            ),
            body: Container(
              margin: EdgeInsets.only(left: 16, right: 16),
              child: ListView(
                  physics: BouncingScrollPhysics(),
                  shrinkWrap: true,
                  children: <Widget>[
                    Padding(padding: EdgeInsets.only(top: 16)),
                    Visibility(
                      visible: _showData,
                      child: Column(
                        children: <Widget>[
                          cardWaktuSholat(),
                          Padding(padding: EdgeInsets.only(top: 16)),
                        ],
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          '$_currentLocation',
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: FontSize.body2,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Spacer(),
                        Text(
                          DateFormat('dd MMMM yyyy').format(DateTime.now()),
                          style: TextStyle(
                            fontSize: FontSize.body2,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(top: 16)),
                    cardJadwalSholat(),
                    Padding(padding: EdgeInsets.only(top: 16)),
                    Visibility(
                      visible: _showData,
                      child: ListView.builder(
                          shrinkWrap: true,
                          physics: BouncingScrollPhysics(),
                          itemCount: 7,
                          itemBuilder: (context, position) {
                            return cardItemSholat(_listDateTime, position);
                          }),
                    ),
                    Center(
                      child: Visibility(
                        visible: _progressIndicator,
                        child: JumpingDotsProgressIndicator(
                          fontSize: FontSize.h3,
                          color: TahDoPallete.green1,
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top: 8)),
                    Visibility(
                      visible: _showData,
                      child: InkWell(
                        onTap: () {},
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        child: Text(
                          'Waktu Sholat Tidak Sesuai?',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: TahDoPallete.green1,
                              fontWeight: FontWeight.bold,
                              fontSize: FontSize.caption),
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top: 32)),
                  ]),
            )));
  }

  Widget cardWaktuSholat() {
    String sholat = _currentSholat;
    String sholatTime = _currentTimeSholat;
    String banner = 'assets/images/bg1.jpg';
    return Container(
      child: Container(
        decoration: BoxDecoration(
            color: Colors.green,
            border: Border.all(color: Colors.black12, width: 0.5),
            borderRadius: BorderRadius.all(Radius.circular(5)),
            boxShadow: [
              BoxShadow(
                  color: Colors.black12, blurRadius: 4, offset: Offset(0, 4)),
            ],
            image: DecorationImage(
              image: AssetImage(banner),
              fit: BoxFit.fill,
            )),
        height: 120,
        width: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Menuju $sholat',
                  style: TextStyle(
                      fontSize: FontSize.h6, color: TahDoPallete.green1),
                ),
                Text(
                  '$sholatTime',
                  style: TextStyle(
                      fontSize: FontSize.h3,
                      color: TahDoPallete.green1,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget cardJadwalSholat() {
    String _fajrData = _fajr;
    String _dhuhrData = _dhuhr;
    String _asrData = _asr;
    String _maghribData = _maghrib;
    String _ishaData = _isha;

    return Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          border: Border.all(color: TahDoPallete.green1, width: 1),
          color: TahDoPallete.green1),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Icon(
                Icons.access_time_rounded,
                color: (_fajrStatus) ? Colors.white : Colors.white70,
              ),
              Padding(padding: EdgeInsets.only(left: 8)),
              Text(
                'Subuh',
                style: TextStyle(
                    fontSize: FontSize.body1,
                    fontWeight: FontWeight.w500,
                    color: (_fajrStatus) ? Colors.white : Colors.white70),
              ),
              Spacer(),
              Align(
                alignment: Alignment.topRight,
                child: Text(
                  _fajrData,
                  style: TextStyle(
                      fontSize: FontSize.body1,
                      fontWeight: FontWeight.w500,
                      color: (_fajrStatus) ? Colors.white : Colors.white70),
                ),
              )
            ],
          ),
          Padding(padding: EdgeInsets.only(top: 4, bottom: 4)),
          Row(
            children: <Widget>[
              Icon(
                Icons.access_time_rounded,
                color: (_dhuhrStatus) ? Colors.white : Colors.white70,
              ),
              Padding(padding: EdgeInsets.only(left: 8)),
              Text(
                'Dhuhur',
                style: TextStyle(
                    fontSize: FontSize.body1,
                    fontWeight: FontWeight.w500,
                    color: (_dhuhrStatus) ? Colors.white : Colors.white70),
              ),
              Spacer(),
              Align(
                alignment: Alignment.topRight,
                child: Text(
                  _dhuhrData,
                  style: TextStyle(
                      fontSize: FontSize.body1,
                      fontWeight: FontWeight.w500,
                      color: (_dhuhrStatus) ? Colors.white : Colors.white70),
                ),
              )
            ],
          ),
          Padding(padding: EdgeInsets.only(top: 4, bottom: 4)),
          Row(
            children: <Widget>[
              Icon(
                Icons.access_time_rounded,
                color: (_asrStatus) ? Colors.white : Colors.white70,
              ),
              Padding(padding: EdgeInsets.only(left: 8)),
              Text(
                'Ashar',
                style: TextStyle(
                    fontSize: FontSize.body1,
                    fontWeight: FontWeight.w500,
                    color: (_asrStatus) ? Colors.white : Colors.white70),
              ),
              Spacer(),
              Align(
                alignment: Alignment.topRight,
                child: Text(
                  _asrData,
                  style: TextStyle(
                      fontSize: FontSize.body1,
                      fontWeight: FontWeight.w500,
                      color: (_asrStatus) ? Colors.white : Colors.white70),
                ),
              )
            ],
          ),
          Padding(padding: EdgeInsets.only(top: 4, bottom: 4)),
          Row(
            children: <Widget>[
              Icon(
                Icons.access_time_rounded,
                color: (_maghribStatus) ? Colors.white : Colors.white70,
              ),
              Padding(padding: EdgeInsets.only(left: 8)),
              Text(
                'Magrib',
                style: TextStyle(
                    fontSize: FontSize.body1,
                    fontWeight: FontWeight.w500,
                    color: (_maghribStatus) ? Colors.white : Colors.white70),
              ),
              Spacer(),
              Align(
                alignment: Alignment.topRight,
                child: Text(
                  _maghribData,
                  style: TextStyle(
                      fontSize: FontSize.body1,
                      fontWeight: FontWeight.w500,
                      color: (_maghribStatus) ? Colors.white : Colors.white70),
                ),
              )
            ],
          ),
          Padding(padding: EdgeInsets.only(top: 4, bottom: 4)),
          Row(
            children: <Widget>[
              Icon(
                Icons.access_time_rounded,
                color: (_ishaStatus) ? Colors.white : Colors.white70,
              ),
              Padding(padding: EdgeInsets.only(left: 8)),
              Text(
                'Isya',
                style: TextStyle(
                    fontSize: FontSize.body1,
                    fontWeight: FontWeight.w500,
                    color: (_ishaStatus) ? Colors.white : Colors.white70),
              ),
              Spacer(),
              Align(
                alignment: Alignment.topRight,
                child: Text(
                  _ishaData,
                  style: TextStyle(
                      fontSize: FontSize.body1,
                      fontWeight: FontWeight.w500,
                      color: (_ishaStatus) ? Colors.white : Colors.white70),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget cardItemSholat(List<sholat.Datetime> listDateTime, var position) {
    String _dateNow = _listDateTime[position].date.gregorian;
    String _hijriNow = _listDateTime[position].date.hijri;
    String _fajrTime = _listDateTime[position].times.fajr;
    String _dhuhrTime = _listDateTime[position].times.dhuhr;
    String _asrTime = _listDateTime[position].times.asr;
    String _maghribTime = _listDateTime[position].times.maghrib;
    String _ishaTime = _listDateTime[position].times.isha;

    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.black12, width: 0.5),
              borderRadius: BorderRadius.all(Radius.circular(5)),
              boxShadow: [
                BoxShadow(
                  color: Colors.black12,
                  blurRadius: 4,
                  offset: Offset(0, 4),
                )
              ]),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      _dateNow,
                      style: TextStyle(
                        fontSize: FontSize.caption,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Visibility(
                    visible: (_dateNow == _currentTime) ? true : false,
                    child: Text(
                      '  Hari Ini',
                      style: TextStyle(
                        fontSize: FontSize.caption,
                        fontWeight: FontWeight.bold,
                        color: TahDoPallete.green1,
                      ),
                    ),
                  ),
                  Spacer(),
                  Align(
                    alignment: Alignment.topRight,
                    child: Text(
                      _hijriNow,
                      style: TextStyle(
                        fontSize: FontSize.caption,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(top: 4, bottom: 8),
                child: Container(
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(color: Colors.black12, width: 1))),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Text(
                        'Subuh',
                        style: TextStyle(
                            fontSize: FontSize.caption,
                            fontWeight: FontWeight.w500),
                      ),
                      Padding(padding: EdgeInsets.only(top: 4)),
                      Text(
                        _fajrTime,
                        style: TextStyle(
                          fontSize: FontSize.caption,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        'Dhuhur',
                        style: TextStyle(
                            fontSize: FontSize.caption,
                            fontWeight: FontWeight.w500),
                      ),
                      Padding(padding: EdgeInsets.only(top: 4)),
                      Text(
                        _dhuhrTime,
                        style: TextStyle(
                          fontSize: FontSize.caption,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        'Ashar',
                        style: TextStyle(
                            fontSize: FontSize.caption,
                            fontWeight: FontWeight.w500),
                      ),
                      Padding(padding: EdgeInsets.only(top: 4)),
                      Text(
                        _asrTime,
                        style: TextStyle(
                          fontSize: FontSize.caption,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        'Maghrib',
                        style: TextStyle(
                            fontSize: FontSize.caption,
                            fontWeight: FontWeight.w500),
                      ),
                      Padding(padding: EdgeInsets.only(top: 4)),
                      Text(
                        _maghribTime,
                        style: TextStyle(
                          fontSize: FontSize.caption,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        'Isya',
                        style: TextStyle(
                            fontSize: FontSize.caption,
                            fontWeight: FontWeight.w500),
                      ),
                      Padding(padding: EdgeInsets.only(top: 4)),
                      Text(
                        _ishaTime,
                        style: TextStyle(
                          fontSize: FontSize.caption,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
        Padding(padding: EdgeInsets.only(top: 16))
      ],
    );
  }
}
