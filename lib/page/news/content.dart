class SoftLaunching {
  String title = 'Soft Launching TAHFIDZTA DOAQU';
  String date = '12 Febuari 2020';
  String content =
      'Alhamdulillah, setelah melewati tahap pengembangan dalam beberapa bulan ini insyaallah layanan TAHFIDZTA DOAQU dapat dinikmati bagi pengguna smartphone android dan dapat diakses melalui web browser di alamat Tahfidzta.Doaqu.or.id. TAHFIDZTA DOAQU hadir dengan beragam fitur yang dibutuhkan umat muslim modern ini, seperti beragam info dan artikel yang bermanfaat.\n\nAplikasi ini dikelola oleh tim IT DOAQU. Jika antum memiliki kritik dan saran untuk menjadikan aplikasi ini lebih baik lagi silakan hubungi kami melalui menu yang ada di aplikasi dan web ini.\n\nSyukran, barakallaahufiikum.';
}