class LocationContent {
  String name1 = 'Cabang 1';
  String locationImgPath1 = 'assets/images/loc1.jpg';
  String location1 = 'Jl. Ngelosari, Sedeng, Gunung Pati - Gunung Pati';
  String contactPerson1 = '+62 821-XXXX-XXXX (Whatsapp)';

  String name2 = 'Cabang 2';
  String locationImgPath2 = 'assets/images/loc2.jpg';
  String location2 = 'Bukit Cemara Residence - Tembalang';
  String contactPerson2 = '+62 821-XXXX-XXXX (Whatsapp)';

  String name3 = 'Cabang 3';
  String locationImgPath3 = 'assets/images/loc3.jpg';
  String location3 = 'Masjid Al-Hidayah Puri Santika - Puri Sartika';
  String contactPerson3 = '+62 821-XXXX-XXXX (Whatsapp)';
}