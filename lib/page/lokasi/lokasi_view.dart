import 'package:flutter/material.dart';
import 'package:tahfidzta_doaqu/base/apps.dart';
import 'package:tahfidzta_doaqu/base/style.dart';
import 'package:tahfidzta_doaqu/page/lokasi/content.dart';
import 'lokasi_model.dart';

class LokasiPage extends StatefulWidget {
  @override
  _LokasiPageState createState() => _LokasiPageState();
}

class _LokasiPageState extends State<LokasiPage> {
  List<LocationModel> _locationList = [];

  @override
  void initState() {
    super.initState();
    print('[Page] Lokasi Yayasan');

    _locationList.add(LocationModel(
        name: LocationContent().name1,
        imgPath: LocationContent().locationImgPath1,
        location: LocationContent().location1,
        contactPerson: LocationContent().contactPerson1));
    _locationList.add(LocationModel(
        name: LocationContent().name2,
        imgPath: LocationContent().locationImgPath2,
        location: LocationContent().location2,
        contactPerson: LocationContent().contactPerson2));
    _locationList.add(LocationModel(
        name: LocationContent().name3,
        imgPath: LocationContent().locationImgPath3,
        location: LocationContent().location3,
        contactPerson: LocationContent().contactPerson3));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          Base.appName,
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: TahDoPallete.green1,
        elevation: 0,
      ),
      body: Container(
        margin: EdgeInsets.only(left: 16, right: 16),
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 16),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Lokasi cabang TAHFIDZTA DOAQU',
                  style: TextStyle(
                    fontSize: FontSize.h6,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 4),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'TAHFIDZTA DOAQU memiliki beberapa cabang yang terletak di: ',
                  style: TextStyle(
                    fontSize: FontSize.body2,
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 16),
              child: ListView.builder(
                shrinkWrap: true,
                physics: BouncingScrollPhysics(),
                itemCount: _locationList.length,
                itemBuilder: (context, position) {
                  return locationContainer(_locationList[position]);
                },
              ),
            ),
          ],
        ),
      ),
    ));
  }

  Widget locationContainer(LocationModel lokasiModel) {
    String name = lokasiModel.name;
    String img = lokasiModel.imgPath;
    String location = lokasiModel.location;
    String contactPerson = lokasiModel.contactPerson;
    return Container(
      child: Column(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Text(
              name,
              style: TextStyle(
                fontSize: FontSize.h6,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 8)),
          Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.black12, width: 1),
                image:
                    DecorationImage(image: AssetImage(img), fit: BoxFit.cover)),
            height: 250,
          ),
          Padding(padding: EdgeInsets.only(top: 8)),
          Align(
            alignment: Alignment.topLeft,
            child: Text(
              'Alamat',
              style: TextStyle(
                fontSize: FontSize.caption,
              ),
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 4)),
          Align(
            alignment: Alignment.topLeft,
            child: Text(
              location,
              style: TextStyle(
                  fontSize: FontSize.body2, fontWeight: FontWeight.w500),
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 8)),
          Align(
            alignment: Alignment.topLeft,
            child: Text(
              'Kontak',
              style: TextStyle(
                fontSize: FontSize.caption,
              ),
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 4)),
          Align(
            alignment: Alignment.topLeft,
            child: Text(
              contactPerson,
              style: TextStyle(
                  fontSize: FontSize.body2, fontWeight: FontWeight.w500),
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 16)),
        ],
      ),
    );
  }
}
