class LocationModel {
  String name;
  String imgPath;
  String location;
  String contactPerson;

  LocationModel({this.name, this.imgPath, this.location, this.contactPerson});
}