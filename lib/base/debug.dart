class Debug {
  void locationData(String latitude, String longitude, String currentLocation, String currentCity) {
    return print('[Debug Location] | Latitude: $latitude | Longitude: $longitude | Current Location: $currentLocation | Current Location: $currentLocation');
  }

  void timeSholatData(var currentTime, var fajr, var dhuhr, var asr, var maghrib, var isha){
    return print('[Debug Time] | Now: $currentTime | Subuh: $fajr | Dhuhur: $dhuhr | Ashar: $asr | Maghrib: $maghrib | Isya: $isha');
  }
}