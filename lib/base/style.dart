import 'package:flutter/material.dart';

class FontSize {
  static double h1 = 96;
  static double h2 = 60;
  static double h3 = 48;
  static double h4 = 34;
  static double h5 = 24;
  static double h6 = 20;
  static double body1 = 16;
  static double body2 = 14;
  static double caption = 12;
  static double overline = 10;
}

class TahDoPallete {
  static Color green1 = Color(0xFF1A9F4E);
  static Color green2 = Color(0xFF61B849);
}