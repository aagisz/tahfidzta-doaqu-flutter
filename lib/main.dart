import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tahfidzta_doaqu/splash_view.dart';
import 'package:tahfidzta_doaqu/base/style.dart';

import 'base/apps.dart';

void main() => runApp(MainActivity());

class MainActivity extends StatelessWidget {
  void systemUIReplace() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: TahDoPallete.green1,
    ));
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  }

  @override
  Widget build(BuildContext context) {
    systemUIReplace();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      color: TahDoPallete.green1,
      title: Base.appName,
      home: Scaffold(
        body: Center(
          child: SplashPage(),
        ),
      ),
    );
  }
}
